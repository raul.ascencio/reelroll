README
======


The project is using `poetry <https://python-poetry.org/>`_ and assumes you're
using python +3.9.


To install the dependencies simply run::

    poetry install

That command will create a new virtual environment and install the dependencies,
once complete you can run the following command to start working in the new env::

    poetry shell

Your prompt may display the name of your current virtualenv.

Other dependencies
==================

We make use of the `cloudscraper` project and use the `nodejs` provider for scrapping therefore you need to install node.

- https://nodejs.org/en/

Running the crawlers
====================

This is a typical scrapy project and just includes two spiders:

- rpc_search: Which fetches content from https://www.crunchyroll.com/ajax/?req=RpcApiSearch_GetSearchCandidates
- videos_alpha: Which fetches from the public URLs https://www.crunchyroll.com/videos/anime/alpha?group=all and https://www.crunchyroll.com/videos/drama/alpha?group=all


Both spiders can be found inside the package `crseries.spiders`.

Assuming you are in the `poetry shell` and inside the folder `./crseries` the crawlers could be run with the following commands::

    $ scrapy crawl videos_alpha -O new-alphabetical-list-spider.csv
    $ scrapy crawl rpc_search -O new-rpc-search-list.json

That will produce a csv file and a json file, with the requested content, inside
the `./crseries/example-files` you can find files from previous run.
