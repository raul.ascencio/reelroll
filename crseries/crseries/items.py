# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from itemloaders.processors import Join, MapCompose


class ShowsAlphabeticalItem(Item):
    """
    Represents an element of the alphabetical list of shows provided by
    crunchyroll.

    The list can be found at:
        - https://www.crunchyroll.com/videos/drama/alpha?group=all
        - https://www.crunchyroll.com/videos/anime/alpha?group=all

    This item represents the DOM for each of links with the show name.

    Here's an example of how those looked like at 2021-02-11::

        <li id="media_group_272977" itemscope="" itemtype="https://schema.org/TVSeries"
            class="hover-bubble group-item" group_id="272977"
            data-contentmedia="...">
            <a token="shows-portraits" itemprop="url" href="/100-teacher-pascal" class="text-link ellipsis">
                100% Teacher Pascal
            </a>
        </li>


    """

    legacy_id = Field(
        input_processor=MapCompose(str.strip, lambda text: text[1:]),
        output_processor=Join(),
    )  # a[href]
    group_id = Field(
        output_processor=Join(),
    )  # a[group_id]
    name = Field(
        input_processor=MapCompose(str.title, str.strip),
        output_processor=Join(),
    )  # a::text
    # TODO: Add fields for other attributes


class ShowsRpcSearchCandidate(ShowsAlphabeticalItem):
    """
    Represents an Series item of the JSON returned by the search URL.

    We know that the series URL is the following:
    https://www.crunchyroll.com/ajax/?req=RpcApiSearch_GetSearchCandidates

    That URL returns a JSON which inside it's attribute data contains a list of
    different type of content that could be found in crunchyroll, for series we
    have elements like the following::

        { "type":"Series",
          "id":"42850",
          "etp_guid":"GY9PJ5KWR",
          "name":"Naruto",
          "img":"...jpg","link":"\/naruto"}

    We know that we can obtain the legacy id or slug and the media group id.
    """

    # TODO: Add fields for other attributes, like name.
