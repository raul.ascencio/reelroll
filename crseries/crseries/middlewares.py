# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

import logging

from scrapy import signals
from scrapy.http import TextResponse

# useful for handling different item types with a single interface
# from itemadapter import is_item, ItemAdapter

import cloudscraper


class CrseriesSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info("Spider opened: %s" % spider.name)


class CrseriesDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info("Spider opened: %s" % spider.name)


# taken from
# https://github.com/clemfromspace/scrapy-cloudflare-middleware/blob/master/scrapy_cloudflare_middleware/middlewares.py
# instead of forking the repo or creating a new package we past the class here.
# We started using this middleware but turns out the library cloudflare-scrape
# doesn't work anymore.  https://github.com/Anorov/cloudflare-scrape/issues/408
# Instead we opted for cloudscraper
class CloudFlareMiddleware:
    """Scrapy middleware to bypass the CloudFlare's anti-bot protection"""

    @staticmethod
    def is_cloudflare_challenge(response):
        """Test if the given response contains the cloudflare's protection"""

        # TODO: In the future we may actually want to test.
        return response.status == 403

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest

        logger = logging.getLogger("cloudflaremiddleware")

        # regardless we know our request will be challenged by cloudflare.
        logger.debug(
            "before response... %s request %s cookies %s",
            response,
            request,
            request.cookies,
        )

        if not self.is_cloudflare_challenge(response):
            logger.debug("response... %s", response)
            return response

        logger.debug(
            "Cloudflare protection detected on %s, trying to bypass...", request.url
        )

        cfscraper = cloudscraper.create_scraper(
            interpreter="nodejs", delay=20, debug=False, allow_brotli=False
        )

        # apparently cloudscrapper doesn't build the cookies dict correctly we
        # just get two
        # https://github.com/VeNoMouS/cloudscraper/blob/229088edf45ad178befeb747ab0b0890e941da55/cloudscraper/__init__.py#L814
        # and we don't get a __cf_bm for example.  We need to do the request
        # manually to fill the cfscraper.cookies object correctly.
        cfscraper_resp = cfscraper.get(request.url)

        # TODO: Handle failures for cfscraper.get
        logger.debug("Cloudflare protection for url %s bypassed...", request.url)

        # trying to build a response from what we got
        # The response from cloudscrapper is a
        # https://requests.readthedocs.io/en/master/api/#requests.Response
        # we can use that object and transform it to a scrapy response object
        # https://docs.scrapy.org/en/latest/topics/request-response.html#response-objects
        # We are also using TextResponse because a simple Response object
        # doesn't allows to call selector methods on it, since they expect a
        # TextResponse class.
        return TextResponse(
            url=cfscraper_resp.url,
            status=cfscraper_resp.status_code,
            body=(cfscraper_resp.text),
            request=request,
            encoding=cfscraper_resp.apparent_encoding,
            headers=dict(cfscraper_resp.headers),
        )
