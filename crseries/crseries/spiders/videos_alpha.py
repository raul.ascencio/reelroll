import scrapy
from scrapy.loader import ItemLoader

from crseries.items import ShowsAlphabeticalItem


class VideosAlphaSpider(scrapy.Spider):
    name = "videos_alpha"
    start_urls = [
        "https://www.crunchyroll.com/videos/drama/alpha?group=all",
        "https://www.crunchyroll.com/videos/anime/alpha?group=all",
    ]

    def parse(self, response):
        items_selector = response.css("li.hover-bubble.group-item")
        for item_sel in items_selector:
            alphaItem = ItemLoader(ShowsAlphabeticalItem(), item_sel)
            alphaItem.add_css("legacy_id", "a::attr(href)")
            alphaItem.add_css("group_id", "::attr(group_id)")
            alphaItem.add_css("name", "a::text")
            yield alphaItem.load_item()
