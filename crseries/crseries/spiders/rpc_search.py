#!/usr/bin/env python3

import json
import re

import scrapy

from crseries.items import ShowsRpcSearchCandidate


class RpcSearchSpider(scrapy.Spider):
    """
    Handles the search json from the crunchyroll search

    The start_urls respond with a JSON that's been "commented" out it
    apparently contains data from all the series that the website offers.
    """

    name = "rpc_search"
    start_urls = [
        ("https://www.crunchyroll.com/ajax/" "?req=RpcApiSearch_GetSearchCandidates")
    ]

    # They add a couple of character to the start and end of the response.
    # TODO: We may want to check these every now and then.
    start_secure_comment = r"\/\*-secure-"
    end_secure_comment = r"\*\/"

    def parse(self, response):
        response_text = re.sub(self.start_secure_comment, "", response.text)
        response_text = re.sub(self.end_secure_comment, "", response_text)
        rpc_search_response = json.loads(response_text)
        for item in rpc_search_response["data"]:
            yield ShowsRpcSearchCandidate(
                legacy_id=item["link"], group_id=item["id"], name=item["name"]
            )
